package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobject.ColorlibFormValidationPage;

import net.thucydides.core.annotations.Step;


public class ColorlibFormValidationSteps {
	
	ColorlibFormValidationPage colorlibFormValidationpage;
	
	@Step
	public void  diligenciarPopupDatosTabla(List<List<String>> data, int id) {
		
		colorlibFormValidationpage.Required(data.get(id).get(0).trim());
		colorlibFormValidationpage.selectSport(data.get(id).get(1).trim());
		colorlibFormValidationpage.MultipleSelect(data.get(id).get(2).trim());
		colorlibFormValidationpage.MultipleSelect(data.get(id).get(3).trim());
		colorlibFormValidationpage.url(data.get(id).get(4).trim());
		colorlibFormValidationpage.email(data.get(id).get(5).trim());
		colorlibFormValidationpage.password(data.get(id).get(6).trim());
		colorlibFormValidationpage.confirmPassword(data.get(id).get(7).trim());
		colorlibFormValidationpage.miniumFieldSize(data.get(id).get(8).trim());
		colorlibFormValidationpage.maxiumFieldSize(data.get(id).get(9).trim());
		colorlibFormValidationpage.number(data.get(id).get(10).trim());
		colorlibFormValidationpage.ip(data.get(id).get(11).trim());
		colorlibFormValidationpage.date(data.get(id).get(12).trim());
		colorlibFormValidationpage.dateEarlier(data.get(id).get(13).trim());
		colorlibFormValidationpage.validate();
	}
	 @Step
	 
	 public void verificarIngresoDatosFormilarioExitoso() {
		 colorlibFormValidationpage.formSinErrores();
	 }
	 
	 @Step
	 
	 public void verificarIngresoDatosFormilarioNoExitoso() {
		 colorlibFormValidationpage.formConErrores();
	 }
	

}
