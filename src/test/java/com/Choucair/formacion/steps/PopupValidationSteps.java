package com.choucair.formacion.steps;
import com.choucair.formacion.pageobject.ColorlibLoginPage;
import com.choucair.formacion.pageobject.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {
	ColorlibLoginPage colorlibloginopage;
	ColorlibMenuPage colorlibmenupage;
	

	
	@Step
	
	public void loginColorlib(String struser, String strpass) {
		//a. Abrir navegador con la url de prueba
		colorlibloginopage.open();
		//b. Ingresar usuario demo 
		//c. Ingresar password demo 
		//d. Click en botón Sign in
		colorlibloginopage.IngresarDatos(struser, strpass);
		//e. Verificar la Autenticación (label en home)
		colorlibloginopage.VerificaHome();	
	}
	
	@Step
	public void ingresarFormValidation() {
		colorlibmenupage.menuFormValidation();
	}
	

}
