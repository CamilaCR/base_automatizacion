package com.choucair.formacion.definition;


import java.util.List;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;
	
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;
	
	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autenticoEnColorlibConUsuarioYClave(String usuario, String clave){
		popupValidationSteps.loginColorlib(usuario, clave);
	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingresoALaFuncionalidadFormsValidation(){
		popupValidationSteps.ingresarFormValidation();
	}

	@When("^Diligencio Formulario Popup Validation$")
	public void diligencioFormularioPopupValidation(DataTable dtDatosForm){
		List<List<String>> data = dtDatosForm.raw();	
		for (int i = 1; i < data.size(); i++) {
			colorlibFormValidationSteps.diligenciarPopupDatosTabla(data, i);
		}
	}

	@Then("^Verifico ingreso exitoso$")
	public void verificoIngresoExitoso(){
		colorlibFormValidationSteps.verificarIngresoDatosFormilarioExitoso();
	}
}
