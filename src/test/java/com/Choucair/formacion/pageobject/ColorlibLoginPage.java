package com.choucair.formacion.pageobject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibLoginPage extends PageObject {
	
	//Campo Usuario
	@FindBy(xpath="//*[@id='login']/form/input[1]")
	public WebElementFacade txtUsername;
	
	//Campo Contrase�a
	@FindBy(xpath="//*[@id='login']/form/input[2]")
	public WebElementFacade txtPassword;
	
	//Bot�n
	@FindBy(xpath="//*[@id='login']/form/button")
	public WebElementFacade btnSingIn;
	
	//Label del Home a verificar
	@FindBy(xpath="//*[@id='bootstrap-admin-template']")
	public WebElementFacade lblHomePpal;
	
	public void IngresarDatos(String strUser, String strPass) {
		findBy("//input[@type='text' and @placeholder='Username']").and().type("hola");
		txtUsername.type(strUser);
		txtPassword.sendKeys(strPass);
		waitFor(2).seconds();
		btnSingIn.click();
	}
	
	public void VerificaHome(){
		waitFor(2).seconds();
		String labelv= "Bootstrap-Admin-Template";
		String strMensaje = lblHomePpal.getText();
		waitFor(2).seconds();
		assertThat(strMensaje, containsString(labelv));	
	}
	

}
